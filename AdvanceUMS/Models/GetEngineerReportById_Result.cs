//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvanceUMS.Models
{
    using System;
    
    public partial class GetEngineerReportById_Result
    {
        public int ID { get; set; }
        public Nullable<int> AllocationId { get; set; }
        public Nullable<int> SiteId { get; set; }
        public string Notes { get; set; }
        public Nullable<System.DateTime> JobEndTime { get; set; }
        public Nullable<int> jobStatus { get; set; }
        public string EngineerSign { get; set; }
        public string CustomerSign { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string EngineerName { get; set; }
        public Nullable<System.DateTime> JobDateTime { get; set; }
        public string SiteName { get; set; }
        public string SiteAddress { get; set; }
        public Nullable<int> City { get; set; }
        public Nullable<int> Country { get; set; }
        public string PostCode { get; set; }
        public string RegistrationNo { get; set; }
        public Nullable<int> JobStatus1 { get; set; }
    }
}
