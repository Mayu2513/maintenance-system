﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdvanceUMS.Models
{
    public class ProductAllocationAPI
    {
        public partial class ProductAllocation
        {
            public int Id { get; set; }
            public System.Guid UserId { get; set; }
            public int ProductId { get; set; }
            public decimal Quantity { get; set; }
            public bool IsDeleted { get; set; }
            public System.DateTime InsertDate { get; set; }
            public System.Guid InsertBy { get; set; }
            public System.DateTime LastModified { get; set; }
            public System.Guid LastModifiedBy { get; set; }
            public string ProductName { get; set; }
            public string UserName { get; set; }
        }
    }
}