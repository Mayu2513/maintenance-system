﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdvanceUMS.Models
{
    public class EngineerReportDetails
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> VisitDate { get; set; }
        public int CompanyId { get; set; }
        public int SiteId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string SiteName { get; set; }
        public string SiteAddress { get; set; }
    }
}