//------------------------------------------------------------------------------
// auto-generated
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// /auto-generated
//------------------------------------------------------------------------------

namespace AdvanceUMS.Models
{
    using System;
    
    public partial class GetBillDetailsById_Result
    {
        public Nullable<int> Id { get; set; }
        public string BillNo { get; set; }
        public Nullable<System.DateTime> BillDate { get; set; }
        public Nullable<int> ReportId { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public Nullable<int> SiteId { get; set; }
        public string SiteName { get; set; }
        public string SiteAddress { get; set; }
        public Nullable<System.DateTime> VisitDate { get; set; }
        public string VATNo { get; set; }
        public decimal VATPer { get; set; }
        public decimal VATAmount { get; set; }
        public decimal Amount { get; set; }
        public decimal TotalAmount { get; set; }
        public string PaymentDue { get; set; }
        public string OtherRemarks { get; set; }
        public Nullable<int> IsDelete { get; set; }
        public Nullable<System.DateTime> EntryDate { get; set; }
        public Nullable<System.DateTime> LastModifyDate { get; set; }
    }
}
