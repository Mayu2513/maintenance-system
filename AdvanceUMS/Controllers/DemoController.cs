﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdvanceUMS.Helper;
using AdvanceUMS.Models;
using System.Web.Security;

namespace AdvanceUMS.Controllers
{
    public class DemoController : Controller
    {

        public DemoController()
        {

        }
        // GET: Demo
        public ActionResult Demo()
        {
            return View();
        }
    }
}