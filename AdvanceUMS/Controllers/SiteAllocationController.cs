﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdvanceUMS.Models;
using AdvanceUMS.Helper;
using System.Web.Http;
using System.Data.SqlClient;
namespace AdvanceUMS.Controllers
{
    [System.Web.Http.RoutePrefix("api/SiteAllocation")]
    public class SiteAllocationController : ApiController
    {
        AdvanceUMSEntities db = new AdvanceUMSEntities();
        GeneralHelper dHelper = new GeneralHelper();
        APIResponse apiResponse;
        string Message = "";
        int Status;

        [System.Web.Http.HttpPost]
        public IHttpActionResult GetAllSiteAllocation(tblUser user)
        {
            try
            {

                Message = AppConstant.MESSAGE_RECORD_NOT_FOUND;
                apiResponse = Common.MakeResponse(AppConstant.STATUS_SUCCESS, Message, null, null, null);
                if (user != null)
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    db.Configuration.ProxyCreationEnabled = false;
                    var siteAllocation = db.WP_GetJobsByUserId(user.ID.ToString()).ToList();

                    //ProductAllocations.Include(p => p.Product.Name).Include(p => p.tblUser.UserName).Where(s => s.UserId == user.ID).ToList();
                    if (siteAllocation != null)
                    {
                        if (siteAllocation.Count > 0)
                        {
                            Message = AppConstant.MESSAGE_RECORD_FOUND;
                            apiResponse = Common.MakeResponse(AppConstant.STATUS_SUCCESS, Message, siteAllocation, null, null);
                        }
                        return Ok(apiResponse);
                    }
                }

            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok(apiResponse);
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult GetAllSiteAllocationDetailsById(SiteAllocation site)
        {
            try
            {

                Message = AppConstant.MESSAGE_RECORD_NOT_FOUND;
                apiResponse = Common.MakeResponse(AppConstant.STATUS_SUCCESS, Message, null, null, null);
                if (site.Id > 0)
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    db.Configuration.ProxyCreationEnabled = false;
                    var siteAllocationDetails = db.GetJobDetailsById(site.Id).FirstOrDefault();
                    var Services = db.GetAllServiceForJobId().ToList();

                    SiteAllocationDetails objSiteAllocationDetails = new SiteAllocationDetails();
                   
                    if(siteAllocationDetails!=null)
                    {
                        objSiteAllocationDetails.AllocationDetails = siteAllocationDetails;                       
                    }
                   
                    objSiteAllocationDetails.Services = Services;
                    //ProductAllocations.Include(p => p.Product.Name).Include(p => p.tblUser.UserName).Where(s => s.UserId == user.ID).ToList();
                    if (objSiteAllocationDetails != null)
                    {
                        if (objSiteAllocationDetails.Services.Count > 0)
                        {
                            Message = AppConstant.MESSAGE_RECORD_FOUND;
                            apiResponse = Common.MakeResponse(AppConstant.STATUS_SUCCESS, Message, objSiteAllocationDetails, null, null);
                        }
                        return Ok(apiResponse);
                    }
                }

            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok(apiResponse);
        }


        [System.Web.Http.HttpPost]
        public IHttpActionResult SaveEngineerReportDetails(EngineerReport engineerReport)
        {
            if (engineerReport != null)
            {
                db.Configuration.LazyLoadingEnabled = false;
                db.Configuration.ProxyCreationEnabled = false;
                db.EngineerReports.Add(engineerReport);              
                var saved = db.SaveChangesAsync();

                if (saved != null && saved.Result > 0)
                {

                    Message = AppConstant.MESSAGE_INSERT;
                    apiResponse = Common.MakeResponse(AppConstant.STATUS_SUCCESS, Message, engineerReport, null, null);

                    return Ok(apiResponse);
                }
            }
            return Ok();
        }
    }
}