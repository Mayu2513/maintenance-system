﻿using AdvanceUMS.Models;
using System.Collections.Generic;

namespace AdvanceUMS.Controllers
{
    internal class SiteAllocationDetails
    {
        public GetJobDetailsById_Result AllocationDetails { get; internal set; }
        public List<GetAllServiceForJobId_Result> Services { get; internal set; }
    }
}