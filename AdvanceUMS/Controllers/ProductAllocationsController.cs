﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdvanceUMS.Models;
using AdvanceUMS.Helper;
using System.Web.Http;
using System.Data.SqlClient;

namespace AdvanceUMS
{
    [System.Web.Http.RoutePrefix("api/ProductAllocations")]
    public class ProductAllocationsController : ApiController
    {
        AdvanceUMSEntities db = new AdvanceUMSEntities();
        GeneralHelper dHelper = new GeneralHelper();
        APIResponse apiResponse;
        string Message = "";
        int Status;

        [System.Web.Http.HttpPost]
        public IHttpActionResult GetAllocatedProducts(tblUser user)
        {
            try
            {

                Message = AppConstant.MESSAGE_RECORD_NOT_FOUND;
                apiResponse = Common.MakeResponse(AppConstant.STATUS_SUCCESS, Message, null, null, null);
                if (user != null)
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    db.Configuration.ProxyCreationEnabled = false;
                    var productsAllocation = db.GetAllProductsByUserId(user.ID.ToString()).ToList();

                    //ProductAllocations.Include(p => p.Product.Name).Include(p => p.tblUser.UserName).Where(s => s.UserId == user.ID).ToList();
                    if (productsAllocation != null)
                    {
                        if (productsAllocation.Count > 0)
                        {
                            Message = AppConstant.MESSAGE_RECORD_FOUND;
                            apiResponse = Common.MakeResponse(AppConstant.STATUS_SUCCESS, Message, productsAllocation, null, null);
                        }
                        return Ok(apiResponse);
                    }
                }

            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok(apiResponse);
        }
      
    }

}
