﻿using AdvanceUMS.Helper;
using AdvanceUMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace AdvanceUMS.Controllers
{
    [System.Web.Http.RoutePrefix("api/Engineer")]
    public class EngineerController : ApiController
    {
        AdvanceUMSEntities db = new AdvanceUMSEntities();
        GeneralHelper dHelper = new GeneralHelper();
        APIResponse apiResponse;
        string Message = "";
        int Status;

       
        [System.Web.Http.HttpPost]
        public IHttpActionResult InsertengineerReport(EngineerReport engineerReport)
        {
            try
            {
                Message = AppConstant.MESSAGE_RECORD_NOT_FOUND;
                apiResponse = Common.MakeResponse(AppConstant.STATUS_SUCCESS, Message, null, null, null);
                if (engineerReport != null)
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    db.Configuration.ProxyCreationEnabled = false;
                    var saved = db.SaveChangesAsync();
                 
                    if (saved != null && saved.Result > 0)
                    {

                        Message = AppConstant.MESSAGE_INSERT;
                        apiResponse = Common.MakeResponse(AppConstant.STATUS_SUCCESS, Message, engineerReport, null, null);

                        return Ok(apiResponse);
                    }
                }

            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok(apiResponse);
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult GetEngineerReport(EngineerReport engineerReport)
        {
            try
            {
                Message = AppConstant.MESSAGE_RECORD_NOT_FOUND;
                apiResponse = Common.MakeResponse(AppConstant.STATUS_SUCCESS, Message, null, null, null);
                if (engineerReport != null)
                {
                    db.Configuration.LazyLoadingEnabled = false;
                    db.Configuration.ProxyCreationEnabled = false;
                    var result = db.GetAllProductsByUserId(engineerReport.ID.ToString()).ToList();

                    
                    if (result != null)
                    {
                        if (result.Count > 0)
                        {
                            Message = AppConstant.MESSAGE_RECORD_FOUND;
                            apiResponse = Common.MakeResponse(AppConstant.STATUS_SUCCESS, Message, result, null, null);
                        }
                        return Ok(apiResponse);
                    }
                }

            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok(apiResponse);
        }
    }
}