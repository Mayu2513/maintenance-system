﻿using AdvanceUMS.Helper;
using AdvanceUMS.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdvanceUMS.Controllers
{   
    [RoutePrefix("api/City")]
    public class CityAPIController : ApiController
    {
        private AdvanceUMSEntities db = new AdvanceUMSEntities();
        APIResponse apiResponse;
        string Message = "";
        int Status;

      
        [HttpGet]
        [Route("GetAllCity")]
        // GET api/<controller>
        public IHttpActionResult Get()
        {
            try
            {               
                    db.Configuration.ProxyCreationEnabled = false;
                    Message = AppConstant.MESSAGE_RECORD_FOUND;
                    apiResponse = Common.MakeResponse(AppConstant.STATUS_SUCCESS, Message, null, db.tblCities, null);
                    return Ok(apiResponse);
               
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}