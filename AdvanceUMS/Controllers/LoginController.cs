﻿using AdvanceUMS.Helper;
using AdvanceUMS.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdvanceUMS
{
    [RoutePrefix("api/Login")]
    public class LoginController : ApiController
    {
        AdvanceUMSEntities db = new AdvanceUMSEntities();
        GeneralHelper dHelper = new GeneralHelper();
        APIResponse apiResponse;
        string Message = "";
        int Status;
       
        [HttpPost]
        // GET api/<controller>/5
        public IHttpActionResult LoginCheck(Login login)
        {
            try
            {
                var password = dHelper.Encrypt(login.Password);
                db.Configuration.LazyLoadingEnabled = false;
                var userLogin = db.tblUsers.FirstOrDefault(s =>
                    s.Email == login.UserName || s.UserName == login.UserName && s.Password == password);
                if (userLogin != null)
                {
                    userLogin.LastLoginDate = DateTime.Now;
                    db.Configuration.ProxyCreationEnabled = false;
                    Message = AppConstant.MESSAGE_RECORD_FOUND;
                    apiResponse = Common.MakeResponse(AppConstant.STATUS_SUCCESS, Message, userLogin, null, null);
                    dHelper.LogAction(userLogin.Email + " successfully login.");
                    return Ok(apiResponse);
                }
                else
                {
                    Message = AppConstant.MESSAGE_RECORD_NOT_FOUND;
                    apiResponse = Common.MakeResponse(AppConstant.STATUS_SUCCESS, Message, null, null, null);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok(apiResponse);
        }           
    }
}