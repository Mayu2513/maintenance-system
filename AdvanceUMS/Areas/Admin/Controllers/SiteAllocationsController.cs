﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdvanceUMS.Models;
using AdvanceUMS.Helper;

namespace AdvanceUMS.Areas.Admin.Controllers
{
    public class SiteAllocationsController : Controller
    {
        private AdvanceUMSEntities db = new AdvanceUMSEntities();
        GeneralHelper dHelper = new GeneralHelper();
        // GET: Admin/SiteAllocations
        public async Task<ActionResult> Index()
        {
            var siteAllocations = db.SiteAllocations.Include(s => s.Site).Include(s => s.tblUser);
            return View(await siteAllocations.ToListAsync());
        }

        // GET: Admin/SiteAllocations/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SiteAllocation siteAllocation = await db.SiteAllocations.FindAsync(id);
            if (siteAllocation == null)
            {
                return HttpNotFound();
            }
            return View(siteAllocation);
        }

        // GET: Admin/SiteAllocations/Create
        public ActionResult Create()
        {
            ViewBag.SiteId = new SelectList(db.Sites, "Id", "Name");
            ViewBag.UserId = new SelectList(db.tblUsers.Where(s => s.IsDeleted != true && s.CompanyId == 1 && s.RoleID.ToString() == "91108935-B324-4466-9CA9-87E5C096B659"), "ID", "FirstName");
            return View();
        }

        // POST: Admin/SiteAllocations/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,UserId,SiteId,JobDateTime,JobStatus,IsDeleted,InsertDate,InsertBy,LastModified,LastModifiedBy")] SiteAllocation siteAllocation)
        {
            if (ModelState.IsValid)
            {
                siteAllocation.IsDeleted = false;
                siteAllocation.InsertDate = DateTime.Now;
                siteAllocation.InsertBy = new Guid(dHelper.CurrentLoginUser());
                siteAllocation.LastModified = DateTime.Now;
                siteAllocation.LastModifiedBy = new Guid(dHelper.CurrentLoginUser());

                db.SiteAllocations.Add(siteAllocation);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.SiteId = new SelectList(db.Sites, "Id", "RegistrationNo", siteAllocation.SiteId);
            ViewBag.UserId = new SelectList(db.tblUsers, "ID", "FirstName", siteAllocation.UserId);
            return View(siteAllocation);
        }

        // GET: Admin/SiteAllocations/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SiteAllocation siteAllocation = await db.SiteAllocations.FindAsync(id);
            if (siteAllocation == null)
            {
                return HttpNotFound();
            }
            ViewBag.SiteId = new SelectList(db.Sites, "Id", "Name", siteAllocation.SiteId);
            ViewBag.UserId = new SelectList(db.tblUsers.Where(s => s.IsDeleted != true && s.CompanyId == 1 && s.RoleID.ToString() == "91108935-B324-4466-9CA9-87E5C096B659"), "ID", "FirstName", siteAllocation.UserId);
            return View(siteAllocation);
        }

        // POST: Admin/SiteAllocations/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,UserId,SiteId,JobDateTime,JobStatus,IsDeleted,InsertDate,InsertBy,LastModified,LastModifiedBy")] SiteAllocation siteAllocation)
        {
            if (ModelState.IsValid)
            {
                siteAllocation.LastModified = DateTime.Now;
                siteAllocation.LastModifiedBy = new Guid(dHelper.CurrentLoginUser());
                db.Entry(siteAllocation).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.SiteId = new SelectList(db.Sites, "Id", "Name", siteAllocation.SiteId);
            ViewBag.UserId = new SelectList(db.tblUsers.Where(s => s.IsDeleted != true && s.CompanyId == 1 && s.RoleID.ToString() == "91108935-B324-4466-9CA9-87E5C096B659"), "ID", "FirstName", siteAllocation.UserId);
            return View(siteAllocation);
        }

        // GET: Admin/SiteAllocations/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SiteAllocation siteAllocation = await db.SiteAllocations.FindAsync(id);
            if (siteAllocation == null)
            {
                return HttpNotFound();
            }
            return View(siteAllocation);
        }

        // POST: Admin/SiteAllocations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            SiteAllocation siteAllocation = await db.SiteAllocations.FindAsync(id);
            db.SiteAllocations.Remove(siteAllocation);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
