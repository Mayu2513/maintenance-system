﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdvanceUMS.Models;

namespace AdvanceUMS.Areas.Admin.Controllers
{
    public class EngineerReportsController : Controller
    {
        private AdvanceUMSEntities db = new AdvanceUMSEntities();

        // GET: Admin/EngineerReports
        public async Task<ActionResult> Index()
        {
            var engineerReports = db.EngineerReports.Include(e => e.SiteAllocation).Include(e => e.Site);
            return View(await engineerReports.ToListAsync());
        }

        // GET: Admin/EngineerReports/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EngineerReport engineerReport = await db.EngineerReports.FindAsync(id);
            if (engineerReport == null)
            {
                return HttpNotFound();
            }
            return View(engineerReport);
        }

        // GET: Admin/EngineerReports/Create
        public ActionResult Create()
        {
            ViewBag.AllocationId = new SelectList(db.SiteAllocations, "Id", "Id");
            ViewBag.SiteId = new SelectList(db.Sites, "Id", "RegistrationNo");
            return View();
        }

        // POST: Admin/EngineerReports/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,AllocationId,SiteId,Notes,JobEndTime,jobStatus,EngineerSign,CustomerSign,IsDeleted")] EngineerReport engineerReport)
        {
            if (ModelState.IsValid)
            {
                db.EngineerReports.Add(engineerReport);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AllocationId = new SelectList(db.SiteAllocations, "Id", "Id", engineerReport.AllocationId);
            ViewBag.SiteId = new SelectList(db.Sites, "Id", "RegistrationNo", engineerReport.SiteId);
            return View(engineerReport);
        }

        // GET: Admin/EngineerReports/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EngineerReport engineerReport = await db.EngineerReports.FindAsync(id);
            if (engineerReport == null)
            {
                return HttpNotFound();
            }
            ViewBag.AllocationId = new SelectList(db.SiteAllocations, "Id", "Id", engineerReport.AllocationId);
            ViewBag.SiteId = new SelectList(db.Sites, "Id", "RegistrationNo", engineerReport.SiteId);
            return View(engineerReport);
        }

        // POST: Admin/EngineerReports/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,AllocationId,SiteId,Notes,JobEndTime,jobStatus,EngineerSign,CustomerSign,IsDeleted")] EngineerReport engineerReport)
        {
            if (ModelState.IsValid)
            {
                db.Entry(engineerReport).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AllocationId = new SelectList(db.SiteAllocations, "Id", "Id", engineerReport.AllocationId);
            ViewBag.SiteId = new SelectList(db.Sites, "Id", "RegistrationNo", engineerReport.SiteId);
            return View(engineerReport);
        }

        // GET: Admin/EngineerReports/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EngineerReport engineerReport = await db.EngineerReports.FindAsync(id);
            if (engineerReport == null)
            {
                return HttpNotFound();
            }
            return View(engineerReport);
        }

        // POST: Admin/EngineerReports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            EngineerReport engineerReport = await db.EngineerReports.FindAsync(id);
            db.EngineerReports.Remove(engineerReport);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
