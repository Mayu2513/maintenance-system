﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdvanceUMS.Models;
using AdvanceUMS.Helper;

namespace AdvanceUMS.Areas.Admin.Controllers
{
    public class ProductAllocationsController : Controller
    {
        private AdvanceUMSEntities db = new AdvanceUMSEntities();
        GeneralHelper dHelper = new GeneralHelper();
        // GET: Admin/ProductAllocations
        public async Task<ActionResult> Index()
        {
            var productAllocations = db.ProductAllocations.Include(p => p.Product).Include(p => p.tblUser).OrderBy(s=>s.UserId);
            return View(await productAllocations.ToListAsync());
        }

        // GET: Admin/ProductAllocations/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductAllocation productAllocation = await db.ProductAllocations.FindAsync(id);
            if (productAllocation == null)
            {
                return HttpNotFound();
            }
            return View(productAllocation);
        }

        // GET: Admin/ProductAllocations/Create
        public ActionResult Create()
        {
            ViewBag.ProductId = new SelectList(db.Products.Where(s => s.IsDeleted != true), "Id", "Name");
            ViewBag.UserId = new SelectList(db.tblUsers.Where(s => s.IsDeleted != true && s.CompanyId == 1 && s.RoleID.ToString() == "91108935-B324-4466-9CA9-87E5C096B659"), "ID", "FirstName");
            return View();
        }

        // POST: Admin/ProductAllocations/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,UserId,ProductId,Quantity,IsDeleted,InsertDate,InsertBy,LastModified,LastModifiedBy")] ProductAllocation productAllocation)
        {
            if (ModelState.IsValid)
            {
                productAllocation.IsDeleted = false;              
                productAllocation.InsertDate = DateTime.Now;
                productAllocation.InsertBy = new Guid(dHelper.CurrentLoginUser());
                productAllocation.LastModified = DateTime.Now;
                productAllocation.LastModifiedBy = new Guid(dHelper.CurrentLoginUser());

                db.ProductAllocations.Add(productAllocation);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ProductId = new SelectList(db.Products, "Id", "Name", productAllocation.ProductId);
            ViewBag.UserId = new SelectList(db.tblUsers.Where(s => s.IsDeleted != true && s.CompanyId == 1 && s.RoleID.ToString() == "91108935-B324-4466-9CA9-87E5C096B659"), "ID", "FirstName", productAllocation.UserId);
            return View(productAllocation);
        }

        // GET: Admin/ProductAllocations/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductAllocation productAllocation = await db.ProductAllocations.FindAsync(id);
            if (productAllocation == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductId = new SelectList(db.Products, "Id", "Name", productAllocation.ProductId);
            ViewBag.UserId = new SelectList(db.tblUsers.Where(s => s.IsDeleted != true && s.CompanyId == 1 && s.RoleID.ToString() == "91108935-B324-4466-9CA9-87E5C096B659"), "ID", "FirstName", productAllocation.UserId);
            return View(productAllocation);
        }

        // POST: Admin/ProductAllocations/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,UserId,ProductId,Quantity,IsDeleted,InsertDate,InsertBy,LastModified,LastModifiedBy")] ProductAllocation productAllocation)
        {
            if (ModelState.IsValid)
            {               
                productAllocation.LastModified = DateTime.Now;
                productAllocation.LastModifiedBy = new Guid(dHelper.CurrentLoginUser());
                db.Entry(productAllocation).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ProductId = new SelectList(db.Products, "Id", "Name", productAllocation.ProductId);
            ViewBag.UserId = new SelectList(db.tblUsers.Where(s => s.IsDeleted != true && s.CompanyId == 1 && s.RoleID.ToString() == "91108935-B324-4466-9CA9-87E5C096B659"), "ID", "FirstName", productAllocation.UserId);
            return View(productAllocation);
        }

        // GET: Admin/ProductAllocations/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductAllocation productAllocation = await db.ProductAllocations.FindAsync(id);
            if (productAllocation == null)
            {
                return HttpNotFound();
            }
            return View(productAllocation);
        }

        // POST: Admin/ProductAllocations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ProductAllocation productAllocation = await db.ProductAllocations.FindAsync(id);
            db.ProductAllocations.Remove(productAllocation);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
