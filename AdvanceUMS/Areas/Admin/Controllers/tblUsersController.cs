﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdvanceUMS.Models;

namespace AdvanceUMS.Areas.Admin.Controllers
{
    public class tblUsersController : Controller
    {
        private AdvanceUMSEntities db = new AdvanceUMSEntities();

        // GET: Admin/tblUsers
        public async Task<ActionResult> Index()
        {
            return View(await db.tblUsers.ToListAsync());
        }

        // GET: Admin/tblUsers/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblUser tblUser = await db.tblUsers.FindAsync(id);
            if (tblUser == null)
            {
                return HttpNotFound();
            }
            return View(tblUser);
        }

        // GET: Admin/tblUsers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/tblUsers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,RoleID,FirstName,LastName,DateOfBirth,Phone,Address,CountryID,Gender,PhotoPath,FacebookLink,TwitterLink,GoogleLink,LinkedInLink,SkypeID,Email,UserName,Password,IsActive,IsDefault,IsLockedOut,IsDeleted,LastLoginDate,LastLockoutDate,FailedPasswordCount,InsertDate,InsertBy,LastModified,LastModifiedBy")] tblUser tblUser)
        {
            if (ModelState.IsValid)
            {
                tblUser.ID = Guid.NewGuid();
                db.tblUsers.Add(tblUser);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(tblUser);
        }

        // GET: Admin/tblUsers/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblUser tblUser = await db.tblUsers.FindAsync(id);
            if (tblUser == null)
            {
                return HttpNotFound();
            }
            return View(tblUser);
        }

        // POST: Admin/tblUsers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,RoleID,FirstName,LastName,DateOfBirth,Phone,Address,CountryID,Gender,PhotoPath,FacebookLink,TwitterLink,GoogleLink,LinkedInLink,SkypeID,Email,UserName,Password,IsActive,IsDefault,IsLockedOut,IsDeleted,LastLoginDate,LastLockoutDate,FailedPasswordCount,InsertDate,InsertBy,LastModified,LastModifiedBy")] tblUser tblUser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblUser).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tblUser);
        }

        // GET: Admin/tblUsers/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblUser tblUser = await db.tblUsers.FindAsync(id);
            if (tblUser == null)
            {
                return HttpNotFound();
            }
            return View(tblUser);
        }

        // POST: Admin/tblUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            tblUser tblUser = await db.tblUsers.FindAsync(id);
            db.tblUsers.Remove(tblUser);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
