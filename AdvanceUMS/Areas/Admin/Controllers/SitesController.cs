﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdvanceUMS.Models;
using AdvanceUMS.Helper;

namespace AdvanceUMS.Areas.Admin.Controllers
{
    public class SitesController : Controller
    {
        private AdvanceUMSEntities db = new AdvanceUMSEntities();
        GeneralHelper dHelper = new GeneralHelper();
        // GET: Admin/Sites
        public async Task<ActionResult> Index()
        {
            var sites = db.Sites.Include(s => s.tblCompany).Where(s => s.IsDeleted == false);
            PopulateDropdown();
            return View(await sites.ToListAsync());
        }

        // GET: Admin/Sites/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Site site = await db.Sites.FindAsync(id);
            if (site == null)
            {
                return HttpNotFound();
            }
            return View(site);
        }

        // GET: Admin/Sites/Create
        public ActionResult Create()
        {
            ViewBag.CompanyId = new SelectList(db.tblCompanies.Where(s => s.IsDeleted == false), "Id", "CompanyName");
            PopulateDropdown();
            return View();
        }

        public void PopulateDropdown()
        {
            var country = db.tblCountries.Where(s => s.IsDeleted == false).OrderBy(s => s.Name);
            ViewBag.CountryList = new SelectList(country, "ID", "Name");

            var city = db.tblCities.Where(s => s.IsDeleted == false).OrderBy(s => s.Name);
            ViewBag.CityList = new SelectList(city, "ID", "Name");
        }

        // POST: Admin/Sites/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Site site)
        {
            PopulateDropdown();
            if (ModelState.IsValid)
            {
                site.IsDeleted = false;
                site.InsertDate = DateTime.Now;
                site.InsertBy = new Guid(dHelper.CurrentLoginUser());
                site.LastModified = DateTime.Now;
                site.LastModifiedBy = new Guid(dHelper.CurrentLoginUser());
                db.Sites.Add(site);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CompanyId = new SelectList(db.tblCompanies, "Id", "CompanyName", site.CompanyId);
            return View(site);
        }

        // GET: Admin/Sites/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            PopulateDropdown();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Site site = await db.Sites.FindAsync(id);
            if (site == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyId = new SelectList(db.tblCompanies.Where(s => s.IsDeleted == false), "Id", "CompanyName", site.CompanyId);
            return View(site);
        }

        // POST: Admin/Sites/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Site site)
        {
            PopulateDropdown();
            if (ModelState.IsValid)
            {               
                site.LastModified = DateTime.Now;
                site.LastModifiedBy = new Guid(dHelper.CurrentLoginUser());
                site.IsDeleted = false;
                db.Entry(site).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyId = new SelectList(db.tblCompanies.Where(s => s.IsDeleted == false), "Id", "CompanyName", site.CompanyId);
            return View(site);
        }

        // GET: Admin/Sites/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Site site = await db.Sites.FindAsync(id);
            if (site == null)
            {
                return HttpNotFound();
            }
            return View(site);
        }

        // POST: Admin/Sites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Site site = await db.Sites.FindAsync(id);
            db.Sites.Remove(site);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
