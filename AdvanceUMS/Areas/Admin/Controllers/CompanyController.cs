﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdvanceUMS.Helper;
using AdvanceUMS.Models;

namespace AdvanceUMS.Areas.Admin.Controllers
{
    public class CompanyController : Controller
    {
        private AdvanceUMSEntities db = new AdvanceUMSEntities();
        GeneralHelper dHelper = new GeneralHelper();
        Dictionary<string, object> res = new Dictionary<string, object>();

        // GET: Admin/tblCompanies
        public ActionResult Index()
        {
            var tblCompanies = db.tblCompanies.Include(t => t.tblCity).Include(t => t.tblCity1).Include(t => t.tblCountry).Include(t => t.tblCountry1).Where(s => s.IsDeleted == false);
            string Url = Request.RawUrl.ToString();
            var control = dHelper.GetUserPermission(Url);
            if (control.IsLogin == false)
                return Redirect("/account/login");
            if (control.IsView == false)
                return Redirect("/admin/notaccess");
            ViewBag.IsAdd = control.IsAdd;
            return View(tblCompanies.ToList());
        }

        // GET: Admin/tblCompanies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblCompany tblCompany = db.tblCompanies.Find(id);
            if (tblCompany == null)
            {
                return HttpNotFound();
            }
            return View(tblCompany);
        }

        // GET: Admin/tblCompanies/Create
        public ActionResult Create()
        {
            ViewBag.City = new SelectList(db.tblCities, "ID", "Name");
            ViewBag.CityO = new SelectList(db.tblCities, "ID", "Name");
            ViewBag.Country = new SelectList(db.tblCountries, "ID", "Name");
            ViewBag.CountryO = new SelectList(db.tblCountries, "ID", "Name");
            return View();
        }

        // POST: Admin/tblCompanies/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CompanyName,RegistrationNo,PostCode,Add1,Add2,Area,City,Country,Email1,Email2,PostCodeO,Add1O,Add2O,AreaO,CityO,CountryO,PersonFirstName,PersonMiddleName,PersonLastName,Post,PersonContactNo,PersonEmail,ContactNo1,ContactNo2,IsActive,IsDefault,IsLockedOut,IsDeleted,LastLoginDate,LastLockoutDate,FailedPasswordCount,InsertDate,InsertBy,LastModified,LastModifiedBy")] tblCompany tblCompany)
        {
            if (ModelState.IsValid)
            {
                tblCompany.IsDeleted = false;
                tblCompany.IsActive = true;
                tblCompany.InsertDate = DateTime.Now;
                tblCompany.InsertBy = new Guid(dHelper.CurrentLoginUser());
                tblCompany.LastModified = DateTime.Now;
                tblCompany.LastModifiedBy= new Guid(dHelper.CurrentLoginUser());
                
                db.tblCompanies.Add(tblCompany);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.City = new SelectList(db.tblCities, "ID", "Name", tblCompany.City);
            ViewBag.CityO = new SelectList(db.tblCities, "ID", "Name", tblCompany.CityO);
            ViewBag.Country = new SelectList(db.tblCountries, "ID", "Name", tblCompany.Country);
            ViewBag.CountryO = new SelectList(db.tblCountries, "ID", "Name", tblCompany.CountryO);
            return View(tblCompany);
        }

        public JsonResult AddCompany(tblCompany objCompany)
        {
            try
            {
                tblCompany company;
                if (objCompany.Id == 0)
                {
                    company = new tblCompany();
                }
                else
                {
                    company = db.tblCompanies.FirstOrDefault(s => s.Id == objCompany.Id);
                }
                if (company != null)
                {   
                    company.CompanyName = objCompany.CompanyName;
                    company.RegistrationNo = objCompany.RegistrationNo;
                    company.IsActive = true;
                    company.PostCode = objCompany.PostCode;
                    company.Add1 = objCompany.Add1;
                    company.Add2 = objCompany.Add2;
                    company.Area = objCompany.Area;
                    company.City = objCompany.City;
                    company.Country = objCompany.Country;
                    company.Email1 = objCompany.Email1;
                    company.Email2 = objCompany.Email2;
                    company.PersonFirstName = objCompany.PersonFirstName;
                    company.PersonMiddleName = objCompany.PersonMiddleName;
                    company.PersonLastName = objCompany.PersonLastName;
                    company.Post = objCompany.Post;
                    company.PersonContactNo = objCompany.PersonContactNo;
                    company.IsDefault = false;
                    company.IsDeleted = false;
                    company.PersonEmail = objCompany.PersonEmail;
                    company.ContactNo1 = objCompany.ContactNo1;
                    company.ContactNo2 = objCompany.ContactNo2;
                    
                    
                    string desc = "";
                    if (objCompany.Id == 0)
                    {
                        company.FailedPasswordCount = 0;
                        company.LastLoginDate = DateTime.Now;
                        company.InsertDate = DateTime.Now;
                        company.LastModifiedBy = new Guid(dHelper.CurrentLoginUser());
                        company.IsLockedOut = false;
                        db.tblCompanies.Add(company);
                        desc = ("Insert Comapany : " + company.CompanyName);
                        Session["Add"] = "add";
                    }
                    else
                    {
                        desc = ("Update Company : " + company.CompanyName);
                        Session["Update"] = "update";
                    }
                    db.SaveChanges();
                    dHelper.LogAction(desc);
                    res["status"] = "1";                   
                    res["url"] = "/admin/Company";
                }

            }
            catch
            {
                res["status"] = "0";
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: Admin/tblCompanies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblCompany tblCompany = db.tblCompanies.Find(id);
            if (tblCompany == null)
            {
                return HttpNotFound();
            }
            ViewBag.City = new SelectList(db.tblCities, "ID", "Name", tblCompany.City);
            ViewBag.CityO = new SelectList(db.tblCities, "ID", "Name", tblCompany.CityO);
            ViewBag.Country = new SelectList(db.tblCountries, "ID", "Name", tblCompany.Country);
            ViewBag.CountryO = new SelectList(db.tblCountries, "ID", "Name", tblCompany.CountryO);
            return View(tblCompany);
        }

        // POST: Admin/tblCompanies/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CompanyName,RegistrationNo,PostCode,Add1,Add2,Area,City,Country,Email1,Email2,PostCodeO,Add1O,Add2O,AreaO,CityO,CountryO,PersonFirstName,PersonMiddleName,PersonLastName,Post,PersonContactNo,PersonEmail,ContactNo1,ContactNo2,IsActive,IsDefault,IsLockedOut,IsDeleted,LastLoginDate,LastLockoutDate,FailedPasswordCount,InsertDate,InsertBy,LastModified,LastModifiedBy")] tblCompany tblCompany)
        {
            if (ModelState.IsValid)
            {
                tblCompany.LastModified = DateTime.Now;
                tblCompany.LastModifiedBy = new Guid(dHelper.CurrentLoginUser());

                db.Entry(tblCompany).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.City = new SelectList(db.tblCities, "ID", "Name", tblCompany.City);
            ViewBag.CityO = new SelectList(db.tblCities, "ID", "Name", tblCompany.CityO);
            ViewBag.Country = new SelectList(db.tblCountries, "ID", "Name", tblCompany.Country);
            ViewBag.CountryO = new SelectList(db.tblCountries, "ID", "Name", tblCompany.CountryO);
            return View(tblCompany);
        }

        // GET: Admin/tblCompanies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblCompany tblCompany = db.tblCompanies.Find(id);           
            if (tblCompany != null)
            {
                tblCompany.IsDeleted = true;
                tblCompany.LastModified = DateTime.Now;
                tblCompany.LastModifiedBy = new Guid(dHelper.CurrentLoginUser());
                db.SaveChanges();

                string Description = string.Format("Delete Company [Name: {0}]", tblCompany.CompanyName);
                dHelper.LogAction(Description);
                res["success"] = 0;
                res["message"] = "<div class=\"alert alert-success fade in\">successfully delete Company.</div>";
                string Query = Request.UrlReferrer.Query;
                if (Query == "")
                    res["url"] = "/admin/Company/";
                else
                    res["url"] = "/admin/Company/" + Query;
            }
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // POST: Admin/tblCompanies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblCompany tblCompany = db.tblCompanies.Find(id);
            db.tblCompanies.Remove(tblCompany);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
