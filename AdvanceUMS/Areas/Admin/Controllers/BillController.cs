﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdvanceUMS.Models;
using System.IO;
using AdvanceUMS.Areas.Admin.Common;
using System.Text;
using iText.Kernel.Pdf;
using iText.Html2pdf;
using iText.Kernel.Geom;
using iText.Layout;

namespace AdvanceUMS.Areas.Admin.Controllers
{
    public class BillController : Controller
    {
        private AdvanceUMSEntities db = new AdvanceUMSEntities();

        // GET: Admin/Bill
        public async Task<ActionResult> Index()
        {
            return View(await db.Bills.OrderByDescending(x=>x.EntryDate).ToListAsync());
        }

        // GET: Admin/Bill/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill bill = await db.Bills.FindAsync(id);
            if (bill == null)
            {
                return HttpNotFound();
            }
            return View(bill);
        }

        // GET: Admin/Bill/Create
        public ActionResult Create()
        {
            ViewBag.EngineerReports = new SelectList(db.EngineerReports, "ID", "ID");
            ViewBag.CompanyId = new SelectList(db.tblCompanies.Where(s => s.IsDeleted == false), "Id", "CompanyName");
            ViewBag.SiteId = new SelectList(db.Sites.Where(s => s.IsDeleted == false), "Id", "Name");
            TempData["BillDetails"] = null;
            return View();
        }

        // POST: Admin/Bill/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Bill bill)
        {
            ViewBag.EngineerReports = new SelectList(db.EngineerReports, "ID", "ID");
            ViewBag.CompanyId = new SelectList(db.tblCompanies.Where(s => s.IsDeleted == false), "Id", "CompanyName");
            ViewBag.SiteId = new SelectList(db.Sites.Where(s => s.IsDeleted == false), "Id", "Name");

            if (ModelState.IsValid)
            {
                bill.EntryDate = DateTime.Now;
                bill.LastModifyDate = DateTime.Now;
                bill.IsDelete = 0;
                db.Bills.Add(bill);
                await db.SaveChangesAsync();

                if(bill.Id > 0)
                {
                    List<BillDetail> billDetails = TempData["BillDetails"] as List<BillDetail>;
                    if(billDetails !=null && billDetails.Count>0)
                    {
                        List<BillDetail> lstdel = billDetails != null ? billDetails.FindAll(x => x.IsDelete == 0) as List<BillDetail> : null;

                        if(lstdel!=null)
                        {
                            foreach (var item in lstdel)
                            {
                                item.BillId = bill.Id;
                                item.BillNo = bill.BillNo;
                                item.EntryDate = DateTime.Now;
                                item.LastModifyDate = DateTime.Now;
                                item.IsDelete = 0;
                                db.BillDetails.Add(item);
                                await db.SaveChangesAsync();
                            }
                        }

                    }
                }

                return RedirectToAction("Index");
            }

            return View(bill);
        }

        // GET: Admin/Bill/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill bill = await db.Bills.FindAsync(id);
            if (bill == null)
            {
                return HttpNotFound();
            }
            return View(bill);
        }

        // POST: Admin/Bill/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,BillNo,BillDate,ReportId,CompanyId,CompanyName,CompanyAddress,SiteId,SiteName,SiteAddress,VisitDate,VATNo,VATPer,VATAmount,Amount,TotalAmount,PaymentDue,OtherRemarks,IsDelete,EntryDate,LastModifyDate")] Bill bill)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bill).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(bill);
        }

        // GET: Admin/Bill/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill bill = await db.Bills.FindAsync(id);
            if (bill == null)
            {
                return HttpNotFound();
            }
            return View(bill);
        }

        // POST: Admin/Bill/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Bill bill = await db.Bills.FindAsync(id);
            db.Bills.Remove(bill);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private static string ORIG = "/uploads/input.html";
        private static string OUTPUT_FOLDER = "/myfiles/";
        static void Main(string[] args)
        {

            string pdfDest = OUTPUT_FOLDER + "output.pdf";
            HtmlConverter.ConvertToPdf(new FileStream(ORIG, FileMode.Open, FileAccess.Read, FileShare.Read), new FileStream(pdfDest, FileMode.Create));
        }

        public virtual void Download_PDF(int id)
        {
            try
            {
                if (id > 0)
                {
                    var BillDetails = db.GetBillDetailsById(id).FirstOrDefault();

                    if (BillDetails != null)
                    {
                        var fileContents = System.IO.File.ReadAllText(Server.MapPath("~") + @"Report\Invoice.html");

                        fileContents = fileContents.ToString();
                        fileContents = fileContents.Replace("##RECEIPIENT##", BillDetails.CompanyName.ToString());
                        fileContents = fileContents.Replace("##SITE##", BillDetails.SiteName.ToString());
                        fileContents = fileContents.Replace("##ADDRESS##", BillDetails.SiteAddress.ToString());
                        fileContents = fileContents.Replace("##PAYMENTDUEON##", BillDetails.PaymentDue.ToString());
                        fileContents = fileContents.Replace("##INVOICENO##", BillDetails.BillNo.ToString());
                        fileContents = fileContents.Replace("##INVOICEDATE##", BillDetails.BillDate.ToString());
                        fileContents = fileContents.Replace("##VATNO##", BillDetails.VATNo.ToString());
                        fileContents = fileContents.Replace("##VATPER##", BillDetails.VATPer.ToString());
                        fileContents = fileContents.Replace("##VATAMOUNT##", BillDetails.VATAmount.ToString());
                        fileContents = fileContents.Replace("##AMOUNT##", BillDetails.Amount.ToString());
                        fileContents = fileContents.Replace("##TOTALAMOUNT##", BillDetails.TotalAmount.ToString());

                        var BillInnerDetails = db.GetBillInnerDetailsById(id).ToList();

                        StringBuilder productRows = new StringBuilder();
                        foreach (var item in BillInnerDetails)
                        {
                            productRows.AppendLine("<tr>");
                            productRows.AppendLine("<td style='padding: 5px 0 5px 0px; text-align: center;'>" + item.Quantity + "</td>");
                            productRows.AppendLine("<td style='padding: 5px 0 5px 0px; text-align: center;'>" + item.Description + "</td>");
                            productRows.AppendLine("<td style='padding: 5px 0 5px 0px; text-align: center;'>" + item.UnitPrice + "</td>");
                            productRows.AppendLine("<td style='padding: 5px 0 5px 0px; text-align: center;'>" + item.Amount + "</td>");
                            productRows.AppendLine("</tr>"); 
                        }

                        fileContents = fileContents.Replace("##ProductRows##", productRows.ToString());

                        //Document pdfDoc = new Document();

                        using (FileStream htmlSource = System.IO.File.Open(Server.MapPath("~") + @"Report\Invoice.html", FileMode.Open))
                        {
                            ConverterProperties converterProperties = new ConverterProperties();
                            converterProperties.SetLimitOfLayouts(1);
                            Response.Clear();
                            Response.ContentType = "Application/pdf";
                            Response.AddHeader("Content-Disposition", "attachment; filename=Invoice.pdf");
                            HtmlConverter.ConvertToPdf(fileContents, Response.OutputStream, converterProperties);
                            Response.Flush();
                            Response.Close();
                            Response.End();
                        }

                        ////Document pdfDoc = new Document();
                        //var pdfWriter = PdfWriter.GetHighPrecision(pdfDoc, HttpContext.Response.OutputStream);
                        //pdfDoc.Open();
                        //var htmlWorker = new HTMLWorker(pdfDoc);
                        //htmlWorker.Parse(new StringReader(fileContents));
                        //pdfWriter.CloseStream = false;
                        //pdfDoc.Close();
                        //Response.Buffer = true;
                        //Response.ContentType = "application/pdf";
                        //Response.AddHeader("content-disposition", "attachment;filename=Test.pdf");
                        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        //Response.Write(pdfDoc);
                        //Response.Flush();
                        //Response.End();
                    }
                }
            }
            catch (Exception EX)
            {
                throw EX;
            }
        }

        public JsonResult GetReportDetails(int? id)
        {
            try
            {
                if (id != null && id <= 0)
                {
                    return null;
                }
                EngineerReport engineerReport = db.EngineerReports.Find(id);
                EngineerReportDetails engineerReportDetails = new EngineerReportDetails();
                if (engineerReport == null)
                {
                    return null;
                }
                engineerReportDetails.ID = engineerReport.ID;
                engineerReportDetails.CompanyId = engineerReport.Site.tblCompany.Id;
                engineerReportDetails.CompanyName = engineerReport.Site.tblCompany.CompanyName;
                engineerReportDetails.CompanyAddress = engineerReport.Site.tblCompany.Add1 + "," + engineerReport.Site.tblCompany.Add2;
                engineerReportDetails.SiteId = (int)engineerReport.SiteId;
                engineerReportDetails.SiteName = engineerReport.Site.Name;
                engineerReportDetails.SiteAddress = engineerReport.Site.Add1 + "," + engineerReport.Site.Add2;
                engineerReportDetails.VisitDate = engineerReport.JobEndTime;

                List<BillDetail> billDetails = new List<BillDetail>();
                TempData["BillDetails"] = billDetails;
                return Json(engineerReportDetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                AllFunctions.WriteLogFile(Convert.ToString(ex));
            }
            return null;
        }

        [HttpPost]
        public JsonResult SaveBillDetails(Bill obj)
        {
            var billDetail = new BillDetail();
            var result = new
            {
                status = 0,
            };

            List<BillDetail> lst = TempData["BillDetails"] as List<BillDetail>;
            if (lst == null)
            {
                lst = new List<BillDetail>();
            }
            if (obj != null)
            {
                billDetail.Description = obj.Description;
                billDetail.Quantity = obj.Quantity;
                billDetail.UnitPrice = obj.UnitPrice;
                billDetail.Amount = obj.BAmount;
                billDetail.IsDelete = 0;
                billDetail.Id = (lst.Count + 1);
                lst.Add(billDetail);
                result = new
                {
                    status = 1,
                };
            }

            TempData["BillDetails"] = lst;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBillDetaisList(Bill obj)
        {
            List<BillDetail> lst = new List<BillDetail>();
            lst = TempData["BillDetails"] as List<BillDetail>;
            TempData["BillDetails"] = lst;

            List<BillDetail> lstdel = lst != null ? lst.FindAll(x => x.IsDelete == 0) as List<BillDetail> : null;
            if (lstdel != null)
            {
                obj.BillDetails = lstdel;
                return PartialView("BillDetails", obj);
            }
            else
            {
                return PartialView("BillDetails", obj);
            }
        }

        public ActionResult Calculateamount(decimal vatper)
        {

            try
            {
                var result = new
                {
                    status = 0,
                    vatamount =(decimal)0.0,
                    amount = (decimal)0.0,
                    total = (decimal)0.0
                };
                List<BillDetail> lst = new List<BillDetail>();
                lst = TempData["BillDetails"] as List<BillDetail>;
                TempData["BillDetails"] = lst;

                List<BillDetail> lstdel = lst != null ? lst.FindAll(x => x.IsDelete == 0) as List<BillDetail> : null;
                if (lstdel != null)
                {
                    decimal total, vattotal, amount;
                    amount = lstdel.Sum(x => x.Amount);
                    vattotal = (amount * vatper) / 100;
                    total = amount + vattotal;
                    result = new
                    {
                        status = 1,
                        vatamount = vattotal,
                        amount = amount,
                        total = total
                    };

                }
               
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        

        public JsonResult DeleteDocketDimension(string id)
        {
            List<BillDetail> lst = TempData["OldBillDetails"] == null ? new List<BillDetail>() : TempData["OldBillDetails"] as List<BillDetail>;
            List<BillDetail> currentList = TempData["BillDetails"] == null ? new List<BillDetail>() : TempData["BillDetails"] as List<BillDetail>;


            if (currentList != null)
            {
                BillDetail objDocDim = currentList.Find(x => x.Id == Convert.ToInt32(id));

                if (objDocDim != null)
                {
                    objDocDim.IsDelete = 1;
                    if (!string.IsNullOrWhiteSpace(id) && Convert.ToInt32(id) > 0)
                    {
                        lst.Add(objDocDim);
                    }

                    currentList.Remove(objDocDim);
                    TempData["BillDetails"] = currentList;
                    TempData["OldBillDetails"] = lst;
                }
            }

            var result = new
            {
                status = 1,
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
