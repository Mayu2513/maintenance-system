﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdvanceUMS.Models;
using AdvanceUMS.Helper;

namespace AdvanceUMS.Areas.Admin.Controllers
{
    public class ProductController : Controller
    {
        private AdvanceUMSEntities db = new AdvanceUMSEntities();
        GeneralHelper dHelper = new GeneralHelper();
        // GET: Admin/Product
        public async Task<ActionResult> Index()
        {
            var products = db.Products.Include(p => p.tblCompany);
            PopulateDropdown();
            return View(await products.ToListAsync());
        }

        // GET: Admin/Product/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        public void PopulateDropdown()
        {
            var obj = db.Categories.Where(s => s.IsDeleted == false).OrderBy(s => s.Name);
            ViewBag.CategoryList = new SelectList(obj, "ID", "Name");

        }


        // GET: Admin/Product/Create
        public ActionResult Create()
        {
            ViewBag.CompanyId = new SelectList(db.tblCompanies, "Id", "CompanyName");
            PopulateDropdown();
            return View();
        }

        // POST: Admin/Product/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,CompanyId,CategoryId,Price,Quantity,IsActive,IsDeleted,InsertDate,InsertBy,LastModified,LastModifiedBy")] Product product)
        {
            if (ModelState.IsValid)
            {
                product.IsDeleted = false;
                product.InsertDate = DateTime.Now;
                product.InsertBy = new Guid(dHelper.CurrentLoginUser());
                product.LastModified = DateTime.Now;
                product.LastModifiedBy = new Guid(dHelper.CurrentLoginUser());

                db.Products.Add(product);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CompanyId = new SelectList(db.tblCompanies, "Id", "CompanyName", product.CompanyId);
            return View(product);
        }

        // GET: Admin/Product/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            PopulateDropdown();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyId = new SelectList(db.tblCompanies, "Id", "CompanyName", product.CompanyId);
            return View(product);
        }

        // POST: Admin/Product/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,CompanyId,CategoryId,Price,Quantity,IsActive,IsDeleted,InsertDate,InsertBy,LastModified,LastModifiedBy")] Product product)
        {
            if (ModelState.IsValid)
            {
                product.LastModified = DateTime.Now;
                product.LastModifiedBy = new Guid(dHelper.CurrentLoginUser());
                db.Entry(product).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyId = new SelectList(db.tblCompanies, "Id", "CompanyName", product.CompanyId);
            return View(product);
        }

        // GET: Admin/Product/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Admin/Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Product product = await db.Products.FindAsync(id);
            db.Products.Remove(product);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
