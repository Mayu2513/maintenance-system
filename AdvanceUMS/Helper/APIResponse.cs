﻿using AdvanceUMS.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace AdvanceUMS.Helper
{
    public class APIResponse
    {
        public int? Status { get; set; }
        public string Message { get; set; }
        public Object Data { get; set; }
        public DbSet Dataset { get; set; }

    }
}
public static class Common
{
    public static APIResponse MakeResponse(int? status, string message, Object data, DbSet dataset, Exception e)
    {
        if (e != null)
        {
            WriteLogs(e);
        }

        return new APIResponse
        {
            Status = status,
            Message = message,
            Data = data,
            Dataset = dataset

        };
    }
    public static void WriteLogs(Exception e)
    {
       // Logger.Logger.WriteFile(e.ToString());
    }

    public static void WriteStringLogs(String str)
    {
        //Logger.Logger.WriteFile(str);
    }

    public static bool GetSiteKeyFromRequest(HttpRequestMessage request)
    {
        try
        {          
            var siteKeyHeader = request.Headers.SingleOrDefault(h => h.Key == "SiteKey");
          
            string siteKey = siteKeyHeader.Value.FirstOrDefault().ToString();

            if (!string.IsNullOrWhiteSpace(siteKey))
            {
                string siteKeyInConfig = ConfigurationManager.AppSettings["SiteKey"].ToString();

                return siteKey == siteKeyInConfig;
            }
            else
            {
                return false;
            }

        }
        catch
        {
           
        }

        return false;
    }
}

public class AppConstant
{
    public static int STATUS_FAILED = 0;
    public static int STATUS_SUCCESS = 1;

    public static string MESSAGE_INSERT = "Record Inserted Successfully";
    public static string MESSAGE_UPDATE = "Record Updated Successfully";
    public static string MESSAGE_DELETE = "Record Deleted Successfully";

    public static string MESSAGE_INVALID_ID = "Invalid Id";
    public static string MESSAGE_INVALID_DATA = "Invalid Data";
    public static string MESSAGE_ERROR_OCCURED = "Error occurred";

    public static string MESSAGE_RECORD_NOT_FOUND = "Record not found";
    public static string MESSAGE_RECORD_FOUND = "Record found";

    public static string DateFormat { get; internal set; }
}