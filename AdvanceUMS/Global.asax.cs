﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using AdvanceUMS.App_Start;
using Newtonsoft.Json;

namespace AdvanceUMS
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // Manually installed WebAPI 2.2 after making an MVC project.
           GlobalConfiguration.Configure(WebApiConfig.Register); // NEW way
                                                                 //WebApiConfig.Register(GlobalConfiguration.Configuration); // DEPRECATED
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.Re‌​ferenceLoopHandling = ReferenceLoopHandling.Ignore;
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);


        }
    }
}
